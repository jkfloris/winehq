<?php

/*
    WineHQ Website
    News plugin
    by Jeremy Newman <jnewman@codeweavers.com>
*/

// language override
if (!empty($_GET['lang']) and in_array($_GET['lang'], $config->languages))
    $html->lang = $_GET['lang'];

// Display news based on page params
// The news plugin expects a narrow range of options though...
// ... so scrub out anything except relevant values

// If there are no potential parameters in the URL...
if (!defined('PAGE_PARAMS'))
    $flag = 'default';

// If a specific date is referenced...
else if (preg_match("/[0-9]{10}/", PAGE_PARAMS, $matches))
{
    $item = $matches[0] . '.xml';
    $vars = array();

    // Ensure valid news actually exists for this date
    if (file_exists($config->news_xml_path.'/'.$html->lang.'/'.$item))
    {
        $vars = get_xml_tags($config->news_xml_path.'/'.$html->lang.'/'.$item, array('date', 'title', 'link', 'body'));
        $flag = 'single';
    }
    else if (file_exists($config->news_xml_path.'/'.$config->lang.'/'.$item))
    {
        $vars = get_xml_tags($config->news_xml_path.'/'.$config->lang.'/'.$item, array('date', 'title', 'link', 'body'));
        $flag = 'single';
    }
    else
        $flag = 'default';
}

// If the RSS feed is requested specifically...
else if (preg_match("/rss/", PAGE_PARAMS))
{
    $feed = 'xml';
    $flag = 'rss';
}

// Anything else...
else
    $flag = 'default';

// Now handle the details based on the control flag
switch ($flag)
{
    // single issue view
    case 'single':

        // The single flag guarantees vars already has the news path

        // set last modified
        $html->set_last_modified($vars['date'], true);

        // set open graph tags
        $html->meta_og['title'] = trim($vars['title']);
        $ogmode = preg_replace('/^(.*)({\$root}\/)(wwn|announce)(\/)(.*)$/ms', '$3', $vars['body']);
        switch ($ogmode)
        {
            case 'wwn':
                $html->meta_og['image'] = "https://media.codeweavers.com/pub/crossover/marketing/og/wine-hq-world-wine-news.png";
                break;
            default:
                $html->meta_og['image'] = "https://media.codeweavers.com/pub/crossover/marketing/og/wine-hq-announcement.png";
        }

        // add page title
        $html->page_title .= " - {$html->meta_og['title']}";

        // if link defined, use it in title
        if (!empty($vars['link']))
        {
            $vars['link'] = str_replace("{\$root}", $html->_web_root, $vars['link']);
            $vars['title'] = '<a href="'.$vars['link'].'">'.$vars['title'].'</a>';
        }
        else
        {
            $vars['title'] = '<a href="'.$html->_web_root.'/news/'.basename($item, ".xml").'">'.$vars['title'].'</a>';
        }

        // replace {$root}
        $vars['body'] = str_replace("{\$root}", $html->_web_root, $vars['body']);

        // add to news body
        echo $html->template('base', 'news_row', $vars);
        echo $html->p($html->ahref('<span class="glyphicon glyphicon-backward"></span>', "{$html->_web_root}/news", 'class="btn btn-default"'));
        break;
    // end single issue

    // RSS view
    case 'rss':
        // get list of news items
        $news = get_files($config->news_xml_path."/".$config->lang, $feed);
        $news = array_reverse($news);

        // get last modified
        $f = get_xml_tags($config->news_xml_path.'/'.$html->lang.'/'.$news[0], array('date'));
        $last_modified = strtotime($f['date']);

        // if etag set in header, and matches timestamp, send 304
        if (@strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == $last_modified || trim($_SERVER['HTTP_IF_NONE_MATCH']) == $last_modified)
        {
            header("HTTP/1.1 304 Not Modified");
            exit();
        }

        // clear cache
        $html->clear_buffer();

        // headers
        header("Last-Modified: ".gmdate("D, d M Y H:i:s", $last_modified)." GMT");
        header("Expires: ".gmdate("D, d M Y H:i:s", (time()+86400))." GMT");
        header("Etag: \"{$last_modified}\"");
        header("Pragma: public");
        header("Cache-Control: public, max-age=86400");
        header("Content-Type: application/xml");
        header('Content-Disposition: inline; filename="winehq_news.xml";');

        // rss header
        $rss = array(
            'rss_title' => "{$config->site_name} News",
            'rss_link'  => "{$config->base_url}news/rss/",
            'rss_img'   => "https://media.codeweavers.com/pub/crossover/marketing/og/wine-hq-announcement.png",
            'rss_desc'  => 'News and information about Wine',
            'rss_crt'   => '(C) '.$config->site_name.' '.date("Y", time()),
            'atom_link' => "https://{$_SERVER['SERVER_NAME']}{$_SERVER['PHP_SELF']}rss"
        );
        $rss_head = $html->template('global', 'xml/rss', $rss, 1);
        list($rss_head, $rss_foot) = preg_split('/\{\$rss_rows\}/', $rss_head, 2);
        echo $rss_head;

        // rss rows
        foreach ($news as $c => $item)
        {
            // limit to 10 most recent articles
            if ($c == 10)
                break;
            
            // get data from XML file
            if (file_exists($config->news_xml_path.'/'.$html->lang.'/'.$item))
                $vars = get_xml_tags($config->news_xml_path.'/'.$html->lang.'/'.$item, array('date', 'title', 'link', 'body'));
            else
                $vars = get_xml_tags($config->news_xml_path.'/'.$config->lang.'/'.$item, array('date', 'title', 'link', 'body'));

            // fix filename for urls
            $item = str_replace(".xml", "", $item);

            // compute where to link to if unspecified
            if (!$vars['link'])
                $vars['link'] = $config->base_url.'news/'.$item;

            // replace {$root}
            $vars['link'] = str_replace("{\$root}", $config->base_url, $vars['link']);
            $vars['body'] = str_replace("{\$root}", $config->base_url, $vars['body']);

            // display row
            $rss_row = array(
                'item_title' => strip_tags($vars['title']),
                'item_desc'  => $vars['body'],
                'item_link'  => $vars['link'],
                'item_guid'  => $config->base_url.'?news='.$item,
                'item_date'  => date("r", strtotime($vars['date']))
            );
            echo $html->template('global', 'xml/rss_row', $rss_row, 1);
        }

        // rss footer
        echo $rss_foot;
        exit();
        break;
    // end rss

    // default view
    default:
        // load rss link
        $html->rss_link = "{$config->base_url}news/rss/";

        // max count for posts per page
        $amax = isset_or($_PLUGIN['n'], 25);

        // where are we in news list
        $x = 0;
        if (defined('PAGE_PARAMS') and is_numeric(PAGE_PARAMS))
            $x = PAGE_PARAMS;

        // get list of news items
        $news = get_files($config->news_xml_path."/".$config->lang, "xml");
        $news = array_reverse ($news);
        $total = count($news);

        // loop and display news
        $c = 0;
        foreach ($news as $key => $item)
        {
            // counter
            $c++;

            // do not show posts less than current pos
            if ($x != 1 and $x >= $c)
                continue;

            // get data from XML file
            $vars = array();
            if (file_exists($config->news_xml_path.'/'.$html->lang.'/'.$item))
                $vars = get_xml_tags($config->news_xml_path.'/'.$html->lang.'/'.$item, array('date', 'title', 'link', 'body'));
            else
                $vars = get_xml_tags($config->news_xml_path.'/'.$config->lang.'/'.$item, array('date', 'title', 'link', 'body'));
            
            // if link defined, use it in title
            if (!empty($vars['link']))
            {
                $vars['link'] = str_replace("{\$root}", $html->_web_root, $vars['link']);
                $vars['title'] = '<a href="'.$vars['link'].'">'.$vars['title'].'</a>';
            }
            else
            {
                $vars['title'] = '<a href="'.$html->_web_root.'/news/'.basename($item, ".xml").'">'.$vars['title'].'</a>';
            }
            
            // replace {$root}
            $vars['body'] = str_replace("{\$root}", $html->_web_root, $vars['body']);
            
            // add to news body
            echo $html->template('base', 'news_row', $vars);

            // set last modified
            $html->set_last_modified($vars['date'], true);

            // show only $max
            if ($c - $x == $amax)
                break;
        }
        // end of news loop

        // prev/next links
        if (PAGE != 'home')
        {
            $pages = floor(($total / $amax));
            $prev_link = "";
            $next_link = "";
            // add back to first page link when not first page
            if ($x != 0)
                $prev_link .=  $html->ahref('<span class="glyphicon glyphicon-fast-backward"></span>', $_SERVER['PHP_SELF'], 'class="btn btn-default"')." ";
            // display prev link
            if ($x > 0)
                $prev_link .= $html->ahref('<span class="glyphicon glyphicon-chevron-left"></span>', $_SERVER['PHP_SELF'].($x - $amax), 'class="btn btn-default"');
            // display next link
            if (($x + $amax) < $total)
            {
                $next_link .= $html->ahref('<span class="glyphicon glyphicon-chevron-right"></span>', $_SERVER['PHP_SELF'].($x + $amax), 'class="btn btn-default"');
                // add jump to last page link
                $next_link .= " ".$html->ahref('<span class="glyphicon glyphicon-fast-forward"></span>', $_SERVER['PHP_SELF'].($pages * $amax), 'class="btn btn-default"');
            }
            // add prev/next links
            echo $html->p($html->div($prev_link.' &nbsp; '.$next_link, 'class="center"'));
        }
    // end default
}

?>
