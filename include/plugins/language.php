<?php

/*
    WineHQ Website
    Select Language Plugin
    by Jeremy Newman <jnewman@codeweavers.com>
*/

switch ($_PLUGIN['cmd'])
{
    // list languages
    case "list":
        foreach ($config->languages as $lang)
        {
            $name = $html->trans_val("lang", "lang", $lang);
            $change = $html->trans_val("lang", "change", $lang);
            echo  "<li style=\"background-image: url('{$html->base_url()}/images/lang/{$lang}.png'); background-repeat: no-repeat; padding: 0 0 10px 40px;\">\n".
                  "    <a href=\"{$html->base_url()}/lang/{$lang}\">{$name}</a> &middot; {$change}\n".
                  "</li>\n";
        }
        break;

    // set the language based on the URL
    default:
        // if specified, switch to lang
        if (defined('PAGE_PARAMS') and in_array(PAGE_PARAMS, $config->languages))
        {
            setcookie("lang", PAGE_PARAMS, time()+60*60*24*365, "{$config->base_root}/");
            $html->redirect($config->base_url);
            exit();
        }
}

?>
