<!--TITLE:[Unterstützung]-->

<h1 class="title">Unterstützung</h1>

<p>Es gibt viele Möglichkeiten, Hilfe zur Benutzung von Wine zu erhalten.</p>

<div class="black inverse bold cornerround padding-sm">
    <div class="row">
        <div class="col-xs-2 col-md-1">Option</div>
        <div class="col-xs-10 col-md-11">Mehr zu dieser Option</div>
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="http://www.codeweavers.com"><i class="fas fa-money-bill-alt fa-2x"></i><br>Paid</a>
    </div>
    <div class="col-xs-10 col-md-11">
        <a href="http://www.codeweavers.com">CodeWeavers</a> bietet bezahlte Unterstützung für Wine an.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="https://wiki.winehq.org/FAQ"><i class="fas fa-info-circle fa-2x"></i><br>FAQ</a>
    </div>
    <div class="col-xs-10 col-md-11">
        Die häufigsten Fragen sind schnell beantwortet, nach dem Überfliegen unserer
        <a href="https://wiki.winehq.org/FAQ">FAQ</a>.
        Dies ist die erste Anlaufstelle, die Sie aufsuchen sollten, um zu sehen, ob Ihre Frage bereits
        beantwortet wurde.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="{$root}/documentation"><i class="fas fa-folder-open fa-2x"></i><br>Docs</a>
    </div>
    <div class="col-xs-10 col-md-11">
        Wir empfehlen, unsere online verfügbare <a href="{$root}/documentation">Dokumentation</a> zu lesen,
        um Antworten auf Ihre Fragen zu finden. Ebenfalls von Interesse dürfte unsere
        <a href="https://wiki.winehq.org/Wine_Installation_and_Configuration">Installations- und Konfigurationsanleitung</a>
        sein, die nützliche Informationen besonders für neue Benutzer bereit hält.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="https://wiki.winehq.org/"><i class="fas fa-book fa-2x"></i><br>Wiki</a>
    </div>
    <div class="col-xs-10 col-md-11">
        Auch unser <a href="https://wiki.winehq.org/">Wiki</a> enthält viele Hinweise, die Sie
        auf Ihrer Suche nach Antworten keinesfalls auslassen sollten.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="//forums.winehq.org/"><i class="fas fa-comments fa-2x"></i><br>Forum</a>
    </div>
    <div class="col-xs-10 col-md-11">
        Wir haben eine <a href="//forums.winehq.org/">web</a>/<a href="{$root}/forums">email</a>-basierte
        Gemeinschaft von Wine-Benutzern, die Sie besuchen können, um Fragen zu stellen und Erfahrungen
        mit anderen Anwendern auszutauschen.
        Erkunden Sie auch die <a href="{$root}/forums">Mailinglisten</a>, die ebenfalls diese
        Gemeinschaft ausmachen.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="{$root}/irc"><i class="fas fa-comment-alt fa-2x"></i><br>IRC</a>
    </div>
    <div class="col-xs-10 col-md-11">
        Verbinden Sie sich mit der Wine-Community via Live-Chat auf unserem
        <a href="{$root}/irc">IRC-Kanal</a>.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="//appdb.winehq.org/"><i class="fas fa-database fa-2x"></i><br>AppDB</a>
    </div>
    <div class="col-xs-10 col-md-11">
        Falls Sie Hilfe mit einer bestimmten Anwendung benötigen, ist unsere
        <a href="//appdb.winehq.org/">Applikations-Datenbank</a> ein guter Anlaufpunkt.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="//bugs.winehq.org/"><i class="fas fa-bug fa-2x"></i><br>Bugzilla</a>
    </div>
    <div class="col-xs-10 col-md-11">
        In unserem <a href="//bugs.winehq.org/">Bugzilla Bug-Tracker</a> sind alle Probleme dokumentiert,
        an denen wir bereits gearbeitet haben oder momentan arbeiten.
    </div>
</div>

<p>Bei Problemen mit unserer Webseite, oder wenn Sie einen Tippfehler entdeckt haben,
   kontaktieren Sie unser <a href="mailto:web-admin@winehq.org">Web-Team</a>.</p>
